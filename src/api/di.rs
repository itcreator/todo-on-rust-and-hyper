use crate::api::server::handler::{
    CreateHandler, DeleteHandler, ListHandler, MarkDoneHandler, MarkNotDoneHandler, ReadHandler,
};
use crate::api::server::{ToDoRouter, TodoPreHandler};
use crate::rest::service::{ErrorPreHandler, RequestTransformer};
use std::sync::Arc;

pub struct ApiDiC {
    pub router: Arc<ToDoRouter>,
}

impl ApiDiC {
    #![allow(clippy::too_many_arguments)]
    pub fn new(
        request_transformer: Arc<RequestTransformer>,
        create_handler: Arc<dyn CreateHandler>,
        list_handler: Arc<dyn ListHandler>,
        read_handler: Arc<dyn ReadHandler>,
        delete_handler: Arc<dyn DeleteHandler>,
        mark_done_handler: Arc<dyn MarkDoneHandler>,
        mark_not_done_handler: Arc<dyn MarkNotDoneHandler>,
        error_pre_handler: Arc<ErrorPreHandler>,
    ) -> Self {
        let pre_handler = Arc::new(TodoPreHandler::new(
            request_transformer,
            create_handler,
            list_handler,
            read_handler,
            delete_handler,
            mark_done_handler,
            mark_not_done_handler,
        ));

        let router = Arc::new(ToDoRouter::new(pre_handler, error_pre_handler));

        ApiDiC { router }
    }
}
