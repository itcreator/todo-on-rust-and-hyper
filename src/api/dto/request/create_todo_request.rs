use crate::api::dto::schema::{CreateTodoBody, CreateTodoParams};
use crate::rest::data_type::HttpRequest;

pub struct CreateTodoRequest {
    pub params: CreateTodoParams,
    pub body: CreateTodoBody,
}

impl HttpRequest for CreateTodoRequest {
    type Body = CreateTodoBody;
    type Params = CreateTodoParams;

    fn new(body: CreateTodoBody, params: CreateTodoParams) -> Self {
        Self { body, params }
    }

    fn body(&self) -> &Self::Body {
        &self.body
    }

    fn params(&self) -> &Self::Params {
        &self.params
    }
}
