use crate::api::dto::schema::DeleteTodoParams;
use crate::rest::data_type::{EmptyRequestBody, HttpRequest};

pub struct DeleteTodoRequest {
    pub params: DeleteTodoParams,
    pub body: EmptyRequestBody,
}

impl HttpRequest for DeleteTodoRequest {
    type Body = EmptyRequestBody;
    type Params = DeleteTodoParams;

    fn new(body: EmptyRequestBody, params: DeleteTodoParams) -> Self {
        Self { body, params }
    }

    fn body(&self) -> &Self::Body {
        &self.body
    }

    fn params(&self) -> &Self::Params {
        &self.params
    }
}
