use crate::api::dto::schema::ListTodoParams;
use crate::rest::data_type::{EmptyRequestBody, HttpRequest};

pub struct ListTodoRequest {
    pub params: ListTodoParams,
    pub body: EmptyRequestBody,
}

impl HttpRequest for ListTodoRequest {
    type Body = EmptyRequestBody;
    type Params = ListTodoParams;

    fn new(body: EmptyRequestBody, params: ListTodoParams) -> Self {
        Self { body, params }
    }

    fn body(&self) -> &Self::Body {
        &self.body
    }

    fn params(&self) -> &Self::Params {
        &self.params
    }
}
