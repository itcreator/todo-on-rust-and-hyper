use crate::api::dto::schema::MarkDoneTodoParams;
use crate::rest::data_type::{EmptyRequestBody, HttpRequest};

pub struct MarkDoneTodoRequest {
    pub params: MarkDoneTodoParams,
    pub body: EmptyRequestBody,
}

impl HttpRequest for MarkDoneTodoRequest {
    type Body = EmptyRequestBody;
    type Params = MarkDoneTodoParams;

    fn new(body: EmptyRequestBody, params: MarkDoneTodoParams) -> Self {
        Self { body, params }
    }

    fn body(&self) -> &Self::Body {
        &self.body
    }

    fn params(&self) -> &Self::Params {
        &self.params
    }
}
