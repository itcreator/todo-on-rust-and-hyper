use crate::api::dto::schema::MarkNotDoneTodoParams;
use crate::rest::data_type::{EmptyRequestBody, HttpRequest};

pub struct MarkNotDoneTodoRequest {
    pub params: MarkNotDoneTodoParams,
    pub body: EmptyRequestBody,
}

impl HttpRequest for MarkNotDoneTodoRequest {
    type Body = EmptyRequestBody;
    type Params = MarkNotDoneTodoParams;

    fn new(body: EmptyRequestBody, params: MarkNotDoneTodoParams) -> Self {
        Self { body, params }
    }

    fn body(&self) -> &Self::Body {
        &self.body
    }

    fn params(&self) -> &Self::Params {
        &self.params
    }
}
