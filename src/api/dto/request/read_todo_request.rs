use crate::api::dto::schema::ReadTodoParams;
use crate::rest::data_type::{EmptyRequestBody, HttpRequest};

pub struct ReadTodoRequest {
    pub params: ReadTodoParams,
    pub body: EmptyRequestBody,
}

impl HttpRequest for ReadTodoRequest {
    type Body = EmptyRequestBody;
    type Params = ReadTodoParams;

    fn new(body: EmptyRequestBody, params: ReadTodoParams) -> Self {
        Self { body, params }
    }

    fn body(&self) -> &Self::Body {
        &self.body
    }

    fn params(&self) -> &Self::Params {
        &self.params
    }
}
