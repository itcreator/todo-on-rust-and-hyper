use serde::Deserialize;
use serde_valid::Validate;

#[derive(Debug, Deserialize, Validate)]
pub struct CreateTodoBody {
    pub name: String,
}
