use crate::rest::data_type::{EmptyPathParams, HttpParams};

pub struct CreateTodoParams {
    pub path: EmptyPathParams,
}

impl HttpParams for CreateTodoParams {
    type Path = EmptyPathParams;

    fn new(path: Self::Path) -> Self {
        Self { path }
    }

    fn path(&self) -> &Self::Path {
        &self.path
    }
}
