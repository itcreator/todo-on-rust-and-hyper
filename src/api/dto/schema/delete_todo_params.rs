use crate::api::dto::schema::DeleteTodoPathParams;
use crate::rest::data_type::HttpParams;

pub struct DeleteTodoParams {
    pub path: DeleteTodoPathParams,
}

impl HttpParams for DeleteTodoParams {
    type Path = DeleteTodoPathParams;

    fn new(path: Self::Path) -> Self {
        Self { path }
    }

    fn path(&self) -> &Self::Path {
        &self.path
    }
}
