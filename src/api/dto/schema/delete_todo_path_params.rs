use crate::rest::data_type::HttpPathParams;
use serde::Deserialize;
use serde_valid::Validate;
use uuid::Uuid;

#[derive(Debug, Deserialize, Validate)]
pub struct DeleteTodoPathParams {
    pub id: Uuid,
}

impl HttpPathParams for DeleteTodoPathParams {}
