use crate::rest::data_type::{EmptyPathParams, HttpParams};

pub struct ListTodoParams {
    pub path: EmptyPathParams,
}

impl HttpParams for ListTodoParams {
    type Path = EmptyPathParams;

    fn new(path: Self::Path) -> Self {
        Self { path }
    }

    fn path(&self) -> &Self::Path {
        &self.path
    }
}
