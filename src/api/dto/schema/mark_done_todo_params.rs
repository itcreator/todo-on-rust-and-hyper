use crate::api::dto::schema::mark_done_todo_path_params::MarkDoneTodoPathParams;
use crate::rest::data_type::HttpParams;

pub struct MarkDoneTodoParams {
    pub path: MarkDoneTodoPathParams,
}

impl HttpParams for MarkDoneTodoParams {
    type Path = MarkDoneTodoPathParams;

    fn new(path: Self::Path) -> Self {
        Self { path }
    }

    fn path(&self) -> &Self::Path {
        &self.path
    }
}
