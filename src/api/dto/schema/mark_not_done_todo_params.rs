use crate::api::dto::schema::mark_not_done_todo_path_params::MarkNotDoneTodoPathParams;
use crate::rest::data_type::HttpParams;

pub struct MarkNotDoneTodoParams {
    pub path: MarkNotDoneTodoPathParams,
}

impl HttpParams for MarkNotDoneTodoParams {
    type Path = MarkNotDoneTodoPathParams;

    fn new(path: Self::Path) -> Self {
        Self { path }
    }

    fn path(&self) -> &Self::Path {
        &self.path
    }
}
