use crate::api::dto::schema::ReadTodoPathParams;
use crate::rest::data_type::HttpParams;

pub struct ReadTodoParams {
    pub path: ReadTodoPathParams,
}

impl HttpParams for ReadTodoParams {
    type Path = ReadTodoPathParams;

    fn new(path: Self::Path) -> Self {
        Self { path }
    }

    fn path(&self) -> &Self::Path {
        &self.path
    }
}
