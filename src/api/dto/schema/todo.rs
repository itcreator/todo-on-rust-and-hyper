use crate::rest::data_type::SerializableBody;
use serde::Serialize;
use uuid::Uuid;

#[derive(Debug, Serialize)]
pub struct TodoDto {
    id: Uuid,
    name: String,
    done: bool,
}

impl TodoDto {
    pub fn new(id: uuid::Uuid, name: String, done: bool) -> Self {
        Self { id, name, done }
    }
}

impl SerializableBody for TodoDto {}

impl SerializableBody for Vec<TodoDto> {}
