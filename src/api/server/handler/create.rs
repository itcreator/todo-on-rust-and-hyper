use crate::api::dto::request::CreateTodoRequest;
use crate::rest::data_type::error::{HttpError, NotImplementedError};
use crate::rest::data_type::HttpResponse;

pub trait CreateHandler: Send + Sync {
    fn handle(&self, _req: CreateTodoRequest) -> Result<HttpResponse, HttpError> {
        Err(HttpError::NotImplemented(NotImplementedError::new()))
    }
}
