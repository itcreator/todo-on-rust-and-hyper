use crate::api::dto::request::DeleteTodoRequest;
use crate::rest::data_type::error::{HttpError, NotImplementedError};
use crate::rest::data_type::HttpResponse;

pub trait DeleteHandler: Send + Sync {
    fn handle(&self, _req: DeleteTodoRequest) -> Result<HttpResponse, HttpError> {
        Err(HttpError::NotImplemented(NotImplementedError::new()))
    }
}
