use crate::api::dto::request::MarkDoneTodoRequest;
use crate::rest::data_type::error::{HttpError, NotImplementedError};
use crate::rest::data_type::HttpResponse;

pub trait MarkDoneHandler: Send + Sync {
    fn handle(&self, _req: MarkDoneTodoRequest) -> Result<HttpResponse, HttpError> {
        Err(HttpError::NotImplemented(NotImplementedError::new()))
    }
}
