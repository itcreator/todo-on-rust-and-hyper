use crate::api::dto::request::MarkNotDoneTodoRequest;
use crate::rest::data_type::error::{HttpError, NotImplementedError};
use crate::rest::data_type::HttpResponse;

pub trait MarkNotDoneHandler: Send + Sync {
    fn handle(&self, _req: MarkNotDoneTodoRequest) -> Result<HttpResponse, HttpError> {
        Err(HttpError::NotImplemented(NotImplementedError::new()))
    }
}
