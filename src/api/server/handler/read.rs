use crate::api::dto::request::ReadTodoRequest;
use crate::rest::data_type::error::{HttpError, NotImplementedError};
use crate::rest::data_type::HttpResponse;

pub trait ReadHandler: Send + Sync {
    fn handle(&self, _req: ReadTodoRequest) -> Result<HttpResponse, HttpError> {
        Err(HttpError::NotImplemented(NotImplementedError::new()))
    }
}
