use crate::api::server::handler::{
    CreateHandler, DeleteHandler, ListHandler, MarkDoneHandler, MarkNotDoneHandler, ReadHandler,
};
use crate::rest::data_type::error::HttpError;
use crate::rest::data_type::{HttpResponse, RequestTrait};
use crate::rest::service::RequestTransformer;
use std::collections::HashMap;
use std::sync::Arc;

pub struct TodoPreHandler {
    request_transformer: Arc<RequestTransformer>,
    create_handler: Arc<dyn CreateHandler>,
    list_handler: Arc<dyn ListHandler>,
    read_handler: Arc<dyn ReadHandler>,
    delete_handler: Arc<dyn DeleteHandler>,
    mark_not_done_handler: Arc<dyn MarkNotDoneHandler>,
    mark_done_handler: Arc<dyn MarkDoneHandler>,
}

impl TodoPreHandler {
    pub fn new(
        request_transformer: Arc<RequestTransformer>,
        create_handler: Arc<dyn CreateHandler>,
        list_handler: Arc<dyn ListHandler>,
        read_handler: Arc<dyn ReadHandler>,
        delete_handler: Arc<dyn DeleteHandler>,
        mark_done_handler: Arc<dyn MarkDoneHandler>,
        mark_not_done_handler: Arc<dyn MarkNotDoneHandler>,
    ) -> Self {
        TodoPreHandler {
            request_transformer,
            create_handler,
            list_handler,
            read_handler,
            delete_handler,
            mark_done_handler,
            mark_not_done_handler,
        }
    }

    pub async fn handle_todo_list(
        &self,
        request: impl RequestTrait,
    ) -> Result<HttpResponse, HttpError> {
        let raw_params = HashMap::from([]);
        let req = self
            .request_transformer
            .transform(request, raw_params)
            .await?;

        self.list_handler.handle(req)
    }

    pub async fn handle_todo_create(
        &self,
        request: impl RequestTrait,
    ) -> Result<HttpResponse, HttpError> {
        let raw_params = HashMap::from([]);
        let req = self
            .request_transformer
            .transform(request, raw_params)
            .await?;

        self.create_handler.handle(req)
    }

    pub async fn handle_todo_read(
        &self,
        request: impl RequestTrait,
        id_str: &str,
    ) -> Result<HttpResponse, HttpError> {
        let raw_params = HashMap::from([("id".to_string(), id_str.to_string())]);
        let req = self
            .request_transformer
            .transform(request, raw_params)
            .await?;

        self.read_handler.handle(req)
    }

    pub async fn handle_todo_delete(
        &self,
        request: impl RequestTrait,
        id_str: &str,
    ) -> Result<HttpResponse, HttpError> {
        let raw_params = HashMap::from([("id".to_string(), id_str.to_string())]);
        let req = self
            .request_transformer
            .transform(request, raw_params)
            .await?;

        self.delete_handler.handle(req)
    }

    pub async fn handle_mark_todo_done(
        &self,
        request: impl RequestTrait,
        id_str: &str,
    ) -> Result<HttpResponse, HttpError> {
        let raw_params = HashMap::from([("id".to_string(), id_str.to_string())]);
        let req = self
            .request_transformer
            .transform(request, raw_params)
            .await?;

        self.mark_done_handler.handle(req)
    }

    pub async fn handle_mark_todo_not_done(
        &self,
        request: impl RequestTrait,
        id_str: &str,
    ) -> Result<HttpResponse, HttpError> {
        let raw_params = HashMap::from([("id".to_string(), id_str.to_string())]);
        let req = self
            .request_transformer
            .transform(request, raw_params)
            .await?;

        self.mark_not_done_handler.handle(req)
    }
}
