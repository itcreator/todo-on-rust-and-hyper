use crate::api::server::TodoPreHandler;
use crate::rest::data_type::error::HttpError;
use crate::rest::data_type::{HttpResponse, Request};
use crate::rest::service::{ErrorPreHandler, Router};
use async_trait::async_trait;
use hyper::Method;
use std::sync::Arc;

pub struct ToDoRouter {
    pub todo_pre_handler: Arc<TodoPreHandler>,
    pub error_pre_handler: Arc<ErrorPreHandler>,
}

impl ToDoRouter {
    pub fn new(
        todo_pre_handler: Arc<TodoPreHandler>,
        error_pre_handler: Arc<ErrorPreHandler>,
    ) -> Self {
        ToDoRouter {
            todo_pre_handler,
            error_pre_handler,
        }
    }
}

#[async_trait]
impl Router for ToDoRouter {
    async fn route(&self, request: Request) -> Result<HttpResponse, HttpError> {
        let path = request.uri().path().to_owned();
        let mut segments = Vec::new();

        for s in path.split('/') {
            match s {
                "" | "." => {}
                s => segments.push(s),
            }
        }

        match segments.as_slice() {
            //todo: implement /done, /not-done, DELETE
            ["todo"] => match *request.method() {
                Method::GET => self.todo_pre_handler.handle_todo_list(request).await,
                Method::POST => self.todo_pre_handler.handle_todo_create(request).await,
                _ => {
                    self.error_pre_handler
                        .handle_method_not_allowed(request)
                        .await
                }
            },
            ["todo", id_str] => match *request.method() {
                Method::GET => {
                    self.todo_pre_handler
                        .handle_todo_read(request, id_str)
                        .await
                }
                Method::DELETE => {
                    self.todo_pre_handler
                        .handle_todo_delete(request, id_str)
                        .await
                }
                _ => {
                    self.error_pre_handler
                        .handle_method_not_allowed(request)
                        .await
                }
            },
            ["todo", id_str, "done"] => match *request.method() {
                Method::POST => {
                    self.todo_pre_handler
                        .handle_mark_todo_done(request, id_str)
                        .await
                }
                _ => {
                    self.error_pre_handler
                        .handle_method_not_allowed(request)
                        .await
                }
            },
            ["todo", id_str, "not-done"] => match *request.method() {
                Method::POST => {
                    self.todo_pre_handler
                        .handle_mark_todo_not_done(request, id_str)
                        .await
                }
                _ => {
                    self.error_pre_handler
                        .handle_method_not_allowed(request)
                        .await
                }
            },
            _ => self.error_pre_handler.handle_not_found(request).await,
        }
    }
}
