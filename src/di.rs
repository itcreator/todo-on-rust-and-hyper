use crate::rest::service::Server;
use std::net::SocketAddr;
use std::sync::Arc;

use crate::api::ApiDiC;
use crate::domain::mock::DomainDiMock;
use crate::domain::DomainDiC;
use crate::infrastructure::InfrastructureDiC;
use crate::rest::RestDiC;

pub struct DiC {
    pub server: Arc<Server>,
}

impl DiC {
    pub fn new(server: Arc<Server>) -> Self {
        DiC { server }
    }
}

pub fn di_factory() -> DiC {
    let domain_mocks = DomainDiMock::new();
    let domain_di = DomainDiC::new(
        domain_mocks.todo_reader.clone(),
        domain_mocks.todo_writer.clone(),
    );

    let rest_di = RestDiC::new();

    let infrastructure_di = InfrastructureDiC::new(&domain_di);

    let api_di = ApiDiC::new(
        rest_di.request_transformer,
        infrastructure_di.create_handler,
        infrastructure_di.list_handler,
        infrastructure_di.read_handler,
        infrastructure_di.delete_handler,
        infrastructure_di.mark_done_handler,
        infrastructure_di.mark_not_done_handler,
        rest_di.error_pre_handler,
    );

    let listen = "127.0.0.1:8080";
    let addr: SocketAddr = listen.parse().expect("Unable to parse socket address");

    let server = Arc::new(Server::new(
        addr,
        api_di.router,
        rest_di.response_transformer,
        rest_di.error_handler,
    ));

    DiC::new(server)
}
