use crate::domain::repository::todo::{TodoReader, TodoWriter};
use crate::domain::use_case::CreateTodoUc;
use std::sync::Arc;

pub struct DomainDiC {
    pub create_todo_uc: Arc<CreateTodoUc>,
    pub todo_reader: Arc<dyn TodoReader>,
    pub todo_writer: Arc<dyn TodoWriter>,
}

impl DomainDiC {
    pub fn new(todo_reader: Arc<dyn TodoReader>, todo_writer: Arc<dyn TodoWriter>) -> Self {
        let create_todo_uc = Arc::new(CreateTodoUc::new(todo_writer.clone()));

        DomainDiC {
            create_todo_uc,
            todo_reader,
            todo_writer,
        }
    }
}
