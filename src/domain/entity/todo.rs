use uuid::Uuid;

#[derive(Clone)]
pub struct Todo {
    pub done: bool,
    pub name: String,
    pub id: Uuid,
}

impl Todo {
    pub fn new(name: String) -> Self {
        Self {
            done: false,
            name,
            id: Uuid::new_v4(),
        }
    }

    pub fn mark_done(&mut self) {
        self.done = true;
    }

    pub fn mark_not_done(&mut self) {
        self.done = false;
    }
}
