use crate::domain::mock::repository::TodoRepositoryMock;
use crate::domain::repository::todo::{TodoReader, TodoWriter};
use std::sync::Arc;

pub struct DomainDiMock {
    pub todo_writer: Arc<dyn TodoWriter>,
    pub todo_reader: Arc<dyn TodoReader>,
}

impl DomainDiMock {
    pub fn new() -> Self {
        let todo_repo = Arc::new(TodoRepositoryMock::new());

        Self {
            todo_writer: todo_repo.clone(),
            todo_reader: todo_repo,
        }
    }
}
