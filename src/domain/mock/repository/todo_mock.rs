use crate::domain::entity::Todo;
use crate::domain::repository::todo::{TodoReader, TodoReaderError, TodoWriter, TodoWriterError};
use std::collections::HashMap;
use std::sync::RwLock;
use uuid::Uuid;

pub struct TodoRepositoryMock {
    todos: RwLock<HashMap<Uuid, Todo>>,
}

impl TodoRepositoryMock {
    pub fn new() -> Self {
        Self {
            todos: RwLock::new(HashMap::new()),
        }
    }
}

impl TodoWriter for TodoRepositoryMock {
    fn save(&self, todo: Todo) -> Result<Todo, TodoWriterError> {
        let t = todo.clone();

        self.todos.write().unwrap().insert(todo.id, t);
        Ok(todo)
    }

    fn delete(&self, todo: Todo) -> Result<(), TodoWriterError> {
        self.todos.write().unwrap().remove(&todo.id.clone());

        Ok(())
    }
}

impl TodoReader for TodoRepositoryMock {
    fn find(&self, id: Uuid) -> Result<Option<Todo>, TodoReaderError> {
        Ok(self.todos.read().unwrap().get(&id).map(|t| Todo {
            id: t.id,
            name: t.name.clone(),
            done: t.done,
        }))
    }

    fn find_all(&self) -> Result<HashMap<Uuid, Todo>, TodoReaderError> {
        Ok(self.todos.read().unwrap().clone())
    }
}
