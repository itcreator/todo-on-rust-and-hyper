use crate::domain::entity::Todo;
use std::collections::HashMap;
use std::error::Error;
use std::fmt::Debug;

pub trait TodoWriter: Send + Sync {
    fn save(&self, todo: Todo) -> Result<Todo, TodoWriterError>;
    fn delete(&self, todo: Todo) -> Result<(), TodoWriterError>;
}

pub trait TodoReader: Send + Sync {
    fn find(&self, id: uuid::Uuid) -> Result<Option<Todo>, TodoReaderError>;

    fn find_all(&self) -> Result<HashMap<uuid::Uuid, Todo>, TodoReaderError>;
}

#[derive(thiserror::Error, Debug)]
#[error("Can't save todo data")]
pub struct TodoWriterError(#[source] pub Option<Box<dyn Error>>);

#[derive(thiserror::Error, Debug)]
#[error("Can't read todo data")]
pub struct TodoReaderError(#[source] pub Option<Box<dyn Error>>);

impl From<TodoReaderError> for TodoWriterError {
    fn from(value: TodoReaderError) -> TodoWriterError {
        TodoWriterError(Some(Box::new(value)))
    }
}
