use crate::domain::entity::Todo;
use crate::domain::repository::todo::{TodoWriter, TodoWriterError};
use std::sync::Arc;

pub struct CreateTodoUc {
    todo_writer: Arc<dyn TodoWriter>,
}

impl CreateTodoUc {
    pub fn new(todo_writer: Arc<dyn TodoWriter>) -> Self {
        Self { todo_writer }
    }

    pub fn execute(&self, todo: Todo) -> Result<Todo, CreateError> {
        let res = self.todo_writer.save(todo)?;

        Ok(res)
    }
}

#[derive(thiserror::Error, Debug)]
pub enum CreateError {
    #[error("External user's error")]
    SavingTodoError(
        #[from]
        #[source]
        TodoWriterError,
    ),
}
