use crate::api::server::handler::{
    CreateHandler, DeleteHandler, ListHandler, MarkDoneHandler, MarkNotDoneHandler, ReadHandler,
};
use crate::domain::DomainDiC;
use crate::infrastructure::handler::{
    CreateTodoHandler, DeleteTodoHandler, ListTodoHandler, MarkTodoDoneHandler,
    MarkTodoNotDoneHandler, ReadTodoHandler,
};
use std::sync::Arc;

pub struct InfrastructureDiC {
    pub create_handler: Arc<dyn CreateHandler>,
    pub list_handler: Arc<dyn ListHandler>,
    pub read_handler: Arc<dyn ReadHandler>,
    pub delete_handler: Arc<dyn DeleteHandler>,
    pub mark_done_handler: Arc<dyn MarkDoneHandler>,
    pub mark_not_done_handler: Arc<dyn MarkNotDoneHandler>,
}

impl InfrastructureDiC {
    pub fn new(domain_di: &DomainDiC) -> Self {
        let create_handler = Arc::new(CreateTodoHandler::new(domain_di.create_todo_uc.clone()));
        let read_handler = Arc::new(ReadTodoHandler::new(domain_di.todo_reader.clone()));
        let list_handler = Arc::new(ListTodoHandler::new(domain_di.todo_reader.clone()));
        let delete_handler = Arc::new(DeleteTodoHandler::new(
            domain_di.todo_reader.clone(),
            domain_di.todo_writer.clone(),
        ));
        let mark_done_handler = Arc::new(MarkTodoDoneHandler::new(
            domain_di.todo_reader.clone(),
            domain_di.todo_writer.clone(),
        ));
        let mark_not_done_handler = Arc::new(MarkTodoNotDoneHandler::new(
            domain_di.todo_reader.clone(),
            domain_di.todo_writer.clone(),
        ));

        InfrastructureDiC {
            create_handler,
            read_handler,
            list_handler,
            delete_handler,
            mark_done_handler,
            mark_not_done_handler,
        }
    }
}
