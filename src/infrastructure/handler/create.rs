use crate::api::dto::request::CreateTodoRequest;
use crate::api::dto::schema::TodoDto;
use crate::api::server::handler::CreateHandler;
use crate::domain::entity::Todo;
use crate::domain::use_case::CreateTodoUc;
use crate::rest::data_type::error::HttpError;
use crate::rest::data_type::HttpResponse;
use std::sync::Arc;

pub struct CreateTodoHandler {
    create_todo_uc: Arc<CreateTodoUc>,
}

impl CreateTodoHandler {
    pub fn new(create_todo_uc: Arc<CreateTodoUc>) -> Self {
        CreateTodoHandler { create_todo_uc }
    }
}

impl CreateHandler for CreateTodoHandler {
    fn handle(&self, req: CreateTodoRequest) -> Result<HttpResponse, HttpError> {
        let todo_body = req.body;

        let todo = Todo::new(todo_body.name);

        let todo = self.create_todo_uc.execute(todo)?;

        let todo_dto = TodoDto::new(todo.id, todo.name, todo.done);

        Ok(HttpResponse::new(
            hyper::StatusCode::CREATED,
            Box::new(todo_dto),
        ))
    }
}
