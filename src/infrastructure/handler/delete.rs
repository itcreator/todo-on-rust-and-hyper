use crate::api::dto::request::DeleteTodoRequest;
use crate::api::server::handler::DeleteHandler;
use crate::domain::repository::todo::{TodoReader, TodoWriter};
use crate::rest::data_type::error::{HttpError, NotFoundError};
use crate::rest::data_type::http_response::EmptyBody;
use crate::rest::data_type::HttpResponse;
use std::sync::Arc;

pub struct DeleteTodoHandler {
    todo_reader: Arc<dyn TodoReader>,
    todo_writer: Arc<dyn TodoWriter>,
}

impl DeleteTodoHandler {
    pub fn new(todo_reader: Arc<dyn TodoReader>, todo_writer: Arc<dyn TodoWriter>) -> Self {
        DeleteTodoHandler {
            todo_reader,
            todo_writer,
        }
    }
}

impl DeleteHandler for DeleteTodoHandler {
    fn handle(&self, req: DeleteTodoRequest) -> Result<HttpResponse, HttpError> {
        let todo = self.todo_reader.find(req.params.path.id)?;
        match todo {
            Some(todo) => {
                self.todo_writer.delete(todo)?;
                Ok(HttpResponse::new(
                    hyper::StatusCode::NO_CONTENT,
                    EmptyBody::new_boxed(),
                ))
            }
            None => Err(HttpError::NotFound(NotFoundError::new(
                "ToDo not found".to_string(),
            ))),
        }
    }
}
