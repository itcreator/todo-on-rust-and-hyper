use crate::api::dto::request::ListTodoRequest;
use crate::api::dto::schema::TodoDto;
use crate::api::server::handler::ListHandler;
use crate::domain::repository::todo::TodoReader;
use crate::rest::data_type::error::HttpError;
use crate::rest::data_type::HttpResponse;
use std::sync::Arc;

pub struct ListTodoHandler {
    todo_reader: Arc<dyn TodoReader>,
}

impl ListTodoHandler {
    pub fn new(todo_reader: Arc<dyn TodoReader>) -> Self {
        Self { todo_reader }
    }
}

impl ListHandler for ListTodoHandler {
    fn handle(&self, _req: ListTodoRequest) -> Result<HttpResponse, HttpError> {
        let todos = self.todo_reader.find_all()?;

        let todo_dtos: Vec<TodoDto> = todos
            .into_values()
            .map(|t| TodoDto::new(t.id, t.name, t.done))
            .collect();

        Ok(HttpResponse::new(
            hyper::StatusCode::OK,
            Box::new(todo_dtos),
        ))
    }
}
