use crate::api::dto::request::MarkDoneTodoRequest;
use crate::api::dto::schema::TodoDto;
use crate::api::server::handler::MarkDoneHandler;
use crate::domain::repository::todo::{TodoReader, TodoWriter};
use crate::rest::data_type::error::{HttpError, NotFoundError};
use crate::rest::data_type::HttpResponse;
use std::sync::Arc;

pub struct MarkTodoDoneHandler {
    todo_reader: Arc<dyn TodoReader>,
    todo_writer: Arc<dyn TodoWriter>,
}

impl MarkTodoDoneHandler {
    pub fn new(todo_reader: Arc<dyn TodoReader>, todo_writer: Arc<dyn TodoWriter>) -> Self {
        MarkTodoDoneHandler {
            todo_reader,
            todo_writer,
        }
    }
}

impl MarkDoneHandler for MarkTodoDoneHandler {
    fn handle(&self, req: MarkDoneTodoRequest) -> Result<HttpResponse, HttpError> {
        let todo = self.todo_reader.find(req.params.path.id)?;
        match todo {
            Some(mut todo) => {
                todo.mark_done();
                let todo = self.todo_writer.save(todo)?;

                let todo_dto = TodoDto::new(todo.id, todo.name, todo.done);

                Ok(HttpResponse::new(hyper::StatusCode::OK, Box::new(todo_dto)))
            }
            None => Err(HttpError::NotFound(NotFoundError::new(
                "ToDo not found".to_string(),
            ))),
        }
    }
}
