mod create;
mod delete;
mod list;
mod mark_done;
mod mark_not_done;
mod read;

pub use create::CreateTodoHandler;
pub use delete::DeleteTodoHandler;
pub use list::ListTodoHandler;
pub use mark_done::MarkTodoDoneHandler;
pub use mark_not_done::MarkTodoNotDoneHandler;
pub use read::ReadTodoHandler;

use crate::domain::repository::todo::{TodoReaderError, TodoWriterError};
use crate::domain::use_case::CreateError;
use crate::rest::data_type::error::{HttpError, InternalServerError};

impl From<CreateError> for HttpError {
    fn from(value: CreateError) -> HttpError {
        HttpError::InternalServerError(InternalServerError::new(Box::new(value)))
    }
}

impl From<TodoWriterError> for HttpError {
    fn from(value: TodoWriterError) -> HttpError {
        HttpError::InternalServerError(InternalServerError::new(Box::new(value)))
    }
}

impl From<TodoReaderError> for HttpError {
    fn from(value: TodoReaderError) -> HttpError {
        HttpError::InternalServerError(InternalServerError::new(Box::new(value)))
    }
}
