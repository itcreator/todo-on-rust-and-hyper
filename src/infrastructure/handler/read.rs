use crate::api::dto::request::ReadTodoRequest;
use crate::api::dto::schema::TodoDto;
use crate::api::server::handler::ReadHandler;
use crate::domain::repository::todo::TodoReader;
use crate::rest::data_type::error::{HttpError, NotFoundError};
use crate::rest::data_type::HttpResponse;
use std::sync::Arc;

pub struct ReadTodoHandler {
    todo_reader: Arc<dyn TodoReader>,
}

impl ReadTodoHandler {
    pub fn new(todo_reader: Arc<dyn TodoReader>) -> Self {
        ReadTodoHandler { todo_reader }
    }
}

impl ReadHandler for ReadTodoHandler {
    fn handle(&self, req: ReadTodoRequest) -> Result<HttpResponse, HttpError> {
        let todo = self.todo_reader.find(req.params.path.id)?;
        match todo {
            Some(todo) => {
                let dto = TodoDto::new(todo.id, todo.name, todo.done);

                Ok(HttpResponse::new(hyper::StatusCode::OK, Box::new(dto)))
            }
            None => Err(HttpError::NotFound(NotFoundError::new(
                "ToDo not found".to_string(),
            ))),
        }
    }
}
