mod api;
mod di;
mod domain;
mod infrastructure;
mod rest;

use crate::di::di_factory;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let di = di_factory();
    di.server.serve().await

    //============ Multi server example
    // let addr1: SocketAddr = ([127, 0, 0, 1], 8080).into();
    // let server1 = Server::new(addr1, router.clone());
    //
    // let addr2: SocketAddr = ([127, 0, 0, 1], 3030).into();
    // let server2 = Server::new(addr2, router.clone());
    //
    // let _res = futures::future::join(server1.serve(), server2.serve()).await;
    //
    // Ok(())
}
