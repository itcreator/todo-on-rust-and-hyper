use crate::rest::data_type::SerializableBody;
use serde::{Serialize, Serializer};
use std::collections::HashMap;

pub type ViolationObject = HashMap<String, Violations>;
pub type ViolationArray = Vec<Violations>;

#[derive(Debug, Clone)]
pub enum ViolationItem {
    Object(ViolationObject),
    Array(ViolationArray),
}

impl Serialize for ViolationItem {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Self::Array(a) => serde::Serialize::serialize(a, serializer),
            Self::Object(o) => serde::Serialize::serialize(o, serializer),
        }
    }
}

impl SerializableBody for ViolationItem {}

impl ViolationItem {
    pub fn is_empty(&self) -> bool {
        match self {
            Self::Object(o) => o.is_empty(),
            Self::Array(a) => a.is_empty(),
        }
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct Violations {
    pub errors: Vec<String>,

    pub items: Option<ViolationItem>,
}

impl SerializableBody for Violations {}

impl Violations {
    pub fn new(errors: Vec<String>, items: Option<ViolationItem>) -> Self {
        Self { errors, items }
    }

    pub fn is_empty(&self) -> bool {
        let items_empty = match &self.items {
            None => true,
            Some(e) => e.is_empty(),
        };

        self.errors.is_empty() && items_empty
    }
}

#[cfg(test)]
mod tests {
    use crate::rest::data_type::violation::{ViolationItem, Violations};
    use serde_json::json;
    use std::collections::HashMap;

    #[test]
    fn test_violations_are_empty() {
        {
            let v = Violations::new(vec![], None);
            assert_eq!(true, v.is_empty());
        }

        {
            let v = Violations::new(vec![], Some(ViolationItem::Array(vec![])));
            assert_eq!(true, v.is_empty());
        }
    }

    #[test]
    fn test_violations_are_not_empty() {
        {
            let v = Violations::new(vec!["Some message".to_string()], None);
            assert_eq!(false, v.is_empty());
        }

        {
            let v = Violations::new(
                vec!["Some message".to_string()],
                Some(ViolationItem::Array(vec![Violations::new(
                    vec!["err".to_string(), "another errr".to_string()],
                    None,
                )])),
            );
            assert_eq!(false, v.is_empty());
        }
    }

    #[test]
    fn test_violation_item_is_empty() {
        {
            let b = ViolationItem::Array(vec![]);
            assert_eq!(true, b.is_empty())
        }
        {
            let b = ViolationItem::Object(HashMap::from([]));
            assert_eq!(true, b.is_empty())
        }
    }

    #[test]
    fn test_violation_item_is_not_empty() {
        {
            let b = ViolationItem::Array(vec![Violations::new(vec![], None)]);

            assert_eq!(false, b.is_empty())
        }

        {
            let b = ViolationItem::Object(HashMap::from([(
                "some-field-name".to_string(),
                Violations::new(vec![], None),
            )]));
            assert_eq!(false, b.is_empty())
        }
    }

    #[test]
    fn test_serialize_violation_item() {
        {
            let i = ViolationItem::Array(vec![Violations::new(vec![], None)]);
            let str = serde_json::to_value(&i).unwrap();

            let expected = json!([{
                "errors": [],
                "items": null,
            }]);

            assert_eq!(str, expected);
        }

        {
            let i = ViolationItem::Object(HashMap::from([(
                "some key".to_string(),
                Violations::new(vec![], None),
            )]));

            let str = serde_json::to_value(&i).unwrap();

            let expected = json!({
                "some key": {
                    "errors": [],
                    "items": null,
                }
            });

            assert_eq!(str, expected);
        }

        {
            let i =
                ViolationItem::Array(vec![Violations::new(vec!["some error".to_string()], None)]);
            let str = serde_json::to_value(&i).unwrap();

            let expected = json!([{
                "errors": ["some error"],
                "items": null,
            }]);

            println!("{}", str);
            assert_eq!(str, expected);
        }
    }
}
