use async_trait::async_trait;

use crate::rest::data_type::error::HttpError;
use crate::rest::data_type::{HttpResponse, Request};

#[async_trait]
pub trait Router: Send + Sync {
    async fn route(&self, request: Request) -> Result<HttpResponse, HttpError>;
}
